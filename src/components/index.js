import VueRoll from './vue-roll.vue'

import './vue-roll.less'

VueRoll.install = Vue => {
    Vue.component(VueRoll.name, VueRoll)
};
export default VueRoll
