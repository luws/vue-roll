# vue-roll
一个基于better-scroll实现的横向滚动图片容器


## demo
![demo](https://gitee.com/luws/vue-roll/raw/master/examples/demo.gif)

## 使用方法
```vue
<vue-roll :images="imgs"
          :item-size="{height:'200px'}" >
</vue-roll>


<script>
import VueRoll from '@shenluw/vue-roll'

export default {
    name: 'demo',
    components: {
        VueRoll,
    },
    data() {
        return {
            imgs: ['img url', ...]
        }
    },
}
</script>

```
